
import { expect } from 'chai';
import 'mocha';
import { People } from '../src/mylib';

const data = require('./data/people.json');

describe('Testing our People class', () => {
  it('A new People object should be created', () => {
    const people = new People(data);
    let person = people.next().value;
    expect(person.age).to.equal(38);
    person = people.next().value;
    expect(person.age).to.equal(22);
    person = people.next().value;
    expect(person.age).to.equal(43);
  });
});
