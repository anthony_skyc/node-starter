# ✨✨ Example Node JS Typescript with Test case ✨✨ 

A starter node application written in Typescript. It includes *ESLint* and *Mocha* test case implementation.

The ESLint rules are very strict.  JSDoc is required on each function

### Installation
```
$ npm install
```

## Other Commands


### ESLint
```
$ npm run lint | npm run lint:fix
```

### Test
```
$ npm run test
```

### Build Dev
```
$ npm run build
```

### Build Prod
```
$ npm run build:prod
```
which runs lint, test & build



