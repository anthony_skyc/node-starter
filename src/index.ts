/**
 * This is a starter node app.  Use this project to create
 * any new nodejs applications.
 */
import { Person, People } from './mylib';

const personArr: Person[] = [];

for (let i = 1; i < 100; i++) {
  const p: Person = {
    name: `Person ${i}`,
    age: i,
    gender: i % 2 === 0 ? 'male' : 'female',
  };
  personArr.push(p);
}

const people = new People(personArr);

while (people.next()) {
  console.log(people.name);
}
