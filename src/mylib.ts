/**
 * This represents what any libraries you write should 
 * look like.  Plenty of comments to describe what the library does
 * what it's purpose is and things like if it's called on a schedule,
 * or only run once for things like a data import. 
 * Provide any examples on it's use if possible.
 */
const MIN_LEGAL_AGE = 18;

export interface Person {
  /**
   * Always describe what the properties are used for
   */
  name: string;

  /**
   * Age is used to determine if the user is of legal 
   * age to purchase some items.
   */
  age: number;

  /**
   * 
   */
  gender: 'male' | 'female';
}

/**
 * This class is constructed with a Person
 * interface and provides a method for the application
 * to quickly check if the person is of legal age
 * to purchase restricted items.
 */
export class People implements Iterator<Person> {
  private cursor = 0;
  private people: Person[];

  /**
   * Moves cursor to the next person.
   * @return {IteratorResult<Person>} the next person
   */
  public next(): IteratorResult<Person> {
    return {
      done: this.people[this.cursor] === undefined,
      value: this.people[this.cursor++],
    };
  }

  /**
   * Checks if the person is of legal age.
   * * The legal age is defined by `MIN_LEGAL_AGE`
   * @return {boolean} `true` if the person is >= `MIN_LEGAL_AGE`
   */
  get isOfLegalAge(): boolean {
    return this.people[this.cursor].age >= MIN_LEGAL_AGE;
  }

  /**
   * Expose the age so that it can be updated
   * @param {number} age
   */
  set age(age: number) {
    this.people[this.cursor].age = age;
  }

  /**
   * 
   */
  get age(): number {
    return this.people[this.cursor].age;
  }

  /**
   * 
   */
  get name(): string {
    return this.people[this.cursor].name;
  }

  /**
   * 
   */
  get gender(): string {
    return this.people[this.cursor].gender;
  }

  /**
   * Constructs a new People class
   * @param {Person[]} persons
   */
  constructor(persons: Person[]) {
    this.people = persons;
  }
}
